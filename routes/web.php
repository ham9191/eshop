<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['midlleware'=>'web'], function (){
    Route::get('/', 'FrontController@index')->name('front');
    Route::post('/upload-avatar', 'DashboardController@add_avatar')->name('upload-avatar');
    Route::get('products', 'ProductController@index')->name('products');
   Route::get('contact', 'ProductController@index')->name('contact');
   Route::get('category', 'ProductController@index')->name('category');
   Route::get('/gggproduct', 'ProductController@index')->name('add-to-cart');
   Route::get('product', 'ProductController@index')->name('product');


});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'roll']], function (){
    Route::get('/dashboard', function (){
        return view('admin.index');
    });
    // Route::resource('category', 'CategoryController');
    Route::resource('category', 'CategoryController');
    Route::resource('product', 'ProductController');

});

Route::get('/', 'FrontController@index');
Route::group(['prefix'=>'dashboard', 'midlleware' => 'auth'], function (){
	Route::get('/', 'DashboardController@index')->name('dashboard');
  Route::resource('/add', 'UserProductController');

});

