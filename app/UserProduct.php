<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
    protected $table = 'user_products';
     protected $fillable = ["title",
        "availability",
        "condition",
        "quantity",
        "price",
        "web_id",
        "posters",
        "category_id",
      ];
}
