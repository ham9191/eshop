<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ["title",
        "availability",
        "condition",
        "quantity",
        "price",
        "web_id",
        "posters",
        "category_id",
    ];
}
