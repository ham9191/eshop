<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(5);
        return view('Admin.productList', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('Admin.productAdd', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->except('_token');
        
        $validator = Validator::make($inputs, [
            "title"=>"required",
            "availability"=>"required",
            "condition"=>"required",
            "quantity"=>"required",
            "price"=>"required",
            "web_id"=>"required",
            "category_id"=>"required",

        ]);
        if($request->hasFile('posters')){
            $files = $request->file('posters');
            $file_names = [];
            foreach ($files as $file){
                $new_name = $file->getClientOriginalName();
                array_push($file_names, $new_name);
                $file->move(public_path() . '/images/', $new_name);
            }
            $inputs['posters'] = json_encode($file_names);
        }
        $product = new Product();
        $product->fill($inputs);
        if ($product->save()){
            return redirect('admin/product')->with('New product created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();

        if (view()->exists('admin.productAdd')) {
            return view('admin.productEdit', compact('product', 'categories'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
            ['title' => 'required|max:255|min:3']);

        if ($validator->fails()){
            return redirect()
                ->route('product.edit', ['id' => $id])
                ->withErrors($validator);
        }

        /** @var Category $category */
        $product = Product::find($id);
        $product->title = $request->title;
        $product->availability = $request->availability;
        $product->condition = $request->condition;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->web_id = $request->web_id;
        $product->update();
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
