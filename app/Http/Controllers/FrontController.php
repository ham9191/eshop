<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class FrontController extends Controller
{
public function index(){
    $categories = Category::all();
    $products = Product::all();
    return view('Front.index', compact('categories', 'products'));
}
}
