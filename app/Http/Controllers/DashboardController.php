<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Dashboard;
use App\Category;
use App\User;

class DashboardController extends Controller
{
    public function index(){
    	$user = Auth::user();
        // dd($user);
    	$products = Dashboard::where('user_id', $user->id)->get();
    	$categories = Category::all();
    	if(view()->exists('front.dashboard')){
    		return view('front.dashboard', compact('products', 'categories'));
    	}

    }
    public function add_avatar(Request $request ){
    	$current_user  = User::find($request->user_id);
    	if($request->hasFile('user_avatar')){
    		$file = $request->file('user_avatar');
    		$file->move(public_path() . '/images/', $file->getClientOriginalName());
    		$current_user->avatar = $file->getClientOriginalName();
    		if($current_user->update()){
    			return $file->getClientOriginalName();
    		}
    	}
    }
}
