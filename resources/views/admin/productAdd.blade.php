@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <h1>Add product</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
            {{ @csrf_field() }}

            <div class="form-group">
                <label for="product-name">Product name</label>
                <input name="title" value="{{ old('title') }}" type="text" class="form-control" id="product-name" placeholder="Add new product...">
            </div>

            <div class="form-group">
                <label for="product-availability">Availability</label>
                <select name="availability" class="form-control" id="product-availability">
                    <option selected disabled="disabled">Product availability</option>
                    <option value="0">no</option>
                    <option value="1">yes</option>
                </select>
            </div>

            <div class="form-group">
                <label for="product-condition">Condition</label>
                <select name="condition" class="form-control" id="product-condition">
                    <option selected disabled="disabled">Product condition</option>
                    <option value="0">old</option>
                    <option value="1">new</option>
                </select>
            </div>

            <div class="form-group">
                <label for="product-quantity">Product quantity</label>
                <input name="quantity" value="{{ old('quantity') }}" type="number" class="form-control" id="product-quantity" placeholder="Add product quantity...">
            </div>

            <div class="form-group">
                <label for="product-price">Product price</label>
                <input name="price" value="{{ old('price') }}" type="number" class="form-control" id="product-price" placeholder="Add product price...">
            </div>

            <div class="form-group">
                <label for="product-web-id">Product web-id</label>
                <input name="web_id" value="{{ old('web_id') }}" type="text" class="form-control" id="product-web-id" placeholder="Add product web-id...">
            </div>

            <div class="form-group">
                <label for="product-posters">Product posters</label>
                <input name="posters[]" multiple type="file" class="form-control filestyle" data-buttonName="btn-primary" id="product-posters">
            </div>

            {{--<div class="form-group">--}}
            {{--<label for="brands_list">Select brand</label>--}}
            {{--<select name="brand_id" class="form-control" id="brands_list">--}}
            {{--<option>Select brand</option>--}}
            {{--@foreach($brands as $brand)--}}
            {{--@if($brand->id == old('brand'))--}}
            {{--<option selected value="{{ $brand->id }}">{{ $brand->title }}</option>--}}
            {{--@else--}}
            {{--<option value="{{ $brand->id }}">{{ $brand->title }}</option>--}}
            {{--@endif--}}
            {{--@endforeach--}}
            {{--</select>--}}
            {{--</div>--}}

            <div class="form-group">
                <label for="category_list">Select category</label>
                <select name="category_id" class="form-control" id="category_list">
                    <option>Select category</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                    @endforeach
                </select>
            </div>

            {{--<div class="form-group sub-category-select">--}}
            {{--<label for="sub_cat_list">Select sub-category</label>--}}
            {{--<select name="sub_category_id" class="form-control category-add" id="sub_cat_list">--}}
            {{--</select>--}}
            {{--</div>--}}

            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Add product
            </button>
        </form>
        <br><br>
    </div>

    <br><br><br><br><br>
@endsection
