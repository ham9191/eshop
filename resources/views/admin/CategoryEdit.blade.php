@extends('layouts.app')

@section('content')
    <div class="col-xs-12">
        <h1>Edit category</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('category.update', $category->id)}}" method="post">
            {{ @csrf_field() }}
            {{ method_field('put') }}
            <div class="form-group">
                <label for="category-edit">Edit category</label>
                <input name="title" type="text" class="form-control" id="category-edit" value="{{$category->title}}">
            </div>
            <button type="submit" class="btn btn-success">
                <i class="fa fa-plus" aria-hidden="true"></i>
                Edit category
            </button>
        </form>
    </div>
@endsection