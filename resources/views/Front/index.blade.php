@extends('layouts.front')
@section('title', 'Home')
@section('carousel')
    <div class="container">
        <div id="id_carousel1" class="carousel fade-slider" data-interval="6000">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#id_carousel1" data-slide-to="0" class="slider_indicators active"></li>
                <li data-target="#id_carousel1" data-slide-to="1" class="slider_indicators"></li>
                <li data-target="#id_carousel1" data-slide-to="2" class="slider_indicators"></li>
            </ol>

            <!-- Slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-sm-offset-1 slide_information">
                                <h3>SLIDER-1 TITLE</h3>
                                <h4>Slider-1 short info</h4>
                                <p>
                                    Slider-1 Description <br>
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-6 slide_picture">
                                <img class="img-responsive" src="{{ asset('images/slider.jpg') }}" alt="Image1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-sm-offset-1 slide_information">
                                <h3>SLIDER-2 TITLE</h3>
                                <h4>Slider-2 short info</h4>
                                <p>
                                    Slider-2 Description <br>
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-6 slide_picture">
                                <img class="img-responsive" src="{{ asset('images/slider.jpg') }}" alt="Image2">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-sm-offset-1 slide_information">
                                <h3>SLIDER-3 TITLE</h3>
                                <h4>Slider-3 short info</h4>
                                <p>
                                    Slider-3 Description <br>
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-6 slide_picture">
                                <img class="img-responsive" src="{{ asset('images/slider.jpg') }}" alt="Image3">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Controls -->
            <a href="#id_carousel1" data-slide="prev">
                <i class="fa fa-angle-left left_arrow_carousel1"></i>
            </a>
            <a href="#id_carousel1" data-slide="next">
                <i class="fa fa-angle-right right_arrow_carousel1"></i>
            </a>
        </div>
    </div>
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="col-xs-12 col-sm-9 main_information">
    <!-- Features items -->
    <h3>FEATURES ITEMS</h3>
    <div class="row">
        @foreach($products as $product)
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="product">
                <div class="text-center only_product">
                    <div class="only_product_image">
                        <a href="{{route('product',['id'=>$product->id])}}"><img src="{{ asset('images/'.json_decode($product->posters)[0]) }}" alt="product1"></a>
                    </div>
                    <p>${{$product->price}}</p>
                    <a href="{{route('product',['id'=>$product->id])}}">{{$product->title}}</a>
                </div>
                <div class="select">
                    <form action="{{ route('add-to-cart') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="title" value="{{ $product->title }}">
                        <input type="hidden" name="poster" value="{{ json_decode($product->posters)[0] }}">
                        <input type="hidden" name="web_id" value="{{ $product->web_id }}">
                        <input type="hidden" name="price" value="{{ $product->price }}">
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id ?? null }}">
                        <input type="hidden" class="form-control" name="quantity" id="id_product_details_quantity" value="1"
                               autocomplete="off">
                        @auth
                        <button type="submit" class="btn btn-default btn_add_to_cart">
                        <i class="fa fa-plus-square"></i> Add to cart
                        </button>
                        @endauth
                    </form>
                </div>
            </div>
        </div>
        @endforeach
        {{--<div class="col-xs-12 col-sm-6 col-md-4">--}}
            {{--<div class="product">--}}
                {{--<div class="text-center only_product">--}}
                    {{--<div class="only_product_image">--}}
                        {{--<a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>--}}
                    {{--</div>--}}
                    {{--<p>$119</p>--}}
                    {{--<a href="product_details.html">Product Title</a>--}}
                {{--</div>--}}
                {{--<div class="select">--}}
                    {{--<ul class="nav nav-pills nav-justified">--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-6 col-md-4">--}}
            {{--<div class="product">--}}
                {{--<div class="text-center only_product">--}}
                    {{--<div class="only_product_image">--}}
                        {{--<a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>--}}
                    {{--</div>--}}
                    {{--<p>$56</p>--}}
                    {{--<a href="product_details.html">Product Title</a>--}}
                {{--</div>--}}
                {{--<div class="select">--}}
                    {{--<ul class="nav nav-pills nav-justified">--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-6 col-md-4">--}}
            {{--<div class="product">--}}
                {{--<div class="text-center only_product">--}}
                    {{--<div class="only_product_image">--}}
                        {{--<a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>--}}
                    {{--</div>--}}
                    {{--<p>$140</p>--}}
                    {{--<a href="product_details.html">Product Title</a>--}}
                {{--</div>--}}
                {{--<div class="select">--}}
                    {{--<ul class="nav nav-pills nav-justified">--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-6 col-md-4">--}}
            {{--<div class="product">--}}
                {{--<div class="text-center only_product">--}}
                    {{--<div class="only_product_image">--}}
                        {{--<a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>--}}
                    {{--</div>--}}
                    {{--<p>$142</p>--}}
                    {{--<a href="product_details.html">Product Title</a>--}}
                {{--</div>--}}
                {{--<div class="select">--}}
                    {{--<ul class="nav nav-pills nav-justified">--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-6 col-md-4">--}}
            {{--<div class="product">--}}
                {{--<div class="text-center only_product">--}}
                    {{--<div class="only_product_image">--}}
                        {{--<a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>--}}
                    {{--</div>--}}
                    {{--<p>$73</p>--}}
                    {{--<a href="product_details.html">Product Title</a>--}}
                {{--</div>--}}
                {{--<div class="select">--}}
                    {{--<ul class="nav nav-pills nav-justified">--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to cart</a></li>--}}
                        {{--<li><a href=""><i class="fa fa-plus-square"></i> Add to compare</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    <!-- /Features items -->
    <!-- Recommended items -->
    <h3>RECOMMENDED ITEMS</h3>
    <div id="id_carousel2" class="carousel slide">
        <!-- Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image ">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="text-center carousel_only_product">
                            <div class="carousel_product_image">
                                <a href="#"><img src="{{ asset('images/product.jpg') }}" alt="product1"></a>
                            </div>
                            <p>$56</p>
                            <a href="#">Product Title</a>
                            <button type="button" class="btn btn-default">View</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="" href="#id_carousel2" data-slide="prev">
            <i class="fa fa-angle-left left_arrow_carousel2"></i>
        </a>
        <a class="" href="#id_carousel2" data-slide="next">
            <i class="fa fa-angle-right right_arrow_carousel2"></i>
        </a>
    </div>
    <!-- /Recommended items -->
</div>
@endsection