<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#178EA5">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | IT-TALENTS</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-slider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-touch-slider.css') }}" media="all">
    <link rel="stylesheet" href="{{ asset('owlcarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('owlcarousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('image/favicon.ico') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

{{--@include('layouts.partials._notifications')--}}

<!-- Button to top -->
<div class="to_top" data-spy="affix" data-offset-top="106" id="id_to_top">
    <a href="#"><i class="fa  fa-chevron-up" aria-hidden="true"></i></a>
</div>
<div class="to_top" id="id_to_previous_scroll">
    <a href="#"><i class="fa  fa-chevron-down" aria-hidden="true"></i></a>
</div>
<!-- /Button to top -->

<!-- Header -->
<header>
    <!-- Top line -->
    <div class="top-line">
        <div class="container">
            <div class="row">
                <div class="col-xs-7 top-line_left">
                    <ul class="list-inline">
                        <li><a href="tel:+37499778566"><i class="fa fa-phone" aria-hidden="true"></i> +374 99 77 85 66</a>
                        </li>
                        <li><a href="mailto:info@domain.com"><i class="fa fa-envelope" aria-hidden="true"></i>
                                info@it-talents.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-5 top-line_right">
                    <ul class="nav navbar-nav list-inline pull-right">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /Top line -->
    <!-- Service information -->
    <div class="container">
        <div class="row service">
            <div class="col-xs-12 col-md-4">
                <a href="{{ url('/front') }}"> <img class="logo" src="{{ asset('images/logo.png') }}" alt="logo"> </a>
                <div class="btn-group pull-right clearfix button_usa_dollar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle myBtn"
                                data-toggle="dropdown">
                            USA
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Canada</a></li>
                            <li><a href="#">UK</a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle myBtn"
                                data-toggle="dropdown">
                            DOLLAR
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Canadian Dollar</a></li>
                            <li><a href="#">Pound</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-8 service_menu">
                <ul class="list-inline pull-right">
                    <li><a href="{{ url('dashboard') }}"><i class="fa fa-user" aria-hidden="true"></i> Account</a></li>
                    <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i> Wishlist</a></li>
                    <li><a href="{{ route('dashboard') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart</a></li>
                    @if (Route::has('login'))
                        @auth
                            {{-- Log out --}}
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>
                                Log out
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            {{-- Log out --}}
                        @else
                            {{-- Log in --}}
                        <li>
                            <a id="id_login_button" href="{{ route('login') }}" class="login_register">
                                <i class="fa fa-sign-in" aria-hidden="true"></i> Login
                            </a>
                        </li>
                        <li>
                            <a id="id_register_button" href="{{ route('register') }}" class="login_register">
                                <i class="fa fa-lock" aria-hidden="true"></i> Register
                            </a>
                        </li>
                            {{-- Log in --}}
                            @endauth
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <!-- /Service information -->
    <!-- Category & search -->
    <div class="container category_search" data-spy="affix" data-offset-top="106" id="myNavbar">
        <nav class="navbar navbar-default navbar_main" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header search_div">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navbar_collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <input type="search" class="search_field" placeholder="Search">
                </div>
                <div class="collapse navbar-collapse category" id="navbar_collapse">
                    <ul class="nav navbar-nav">
                        <li class="active custom"><a href="{{ url('/front') }}">Home</a></li>
                        <li><a href="{{ route('products') }}">Products</a></li>
                        <li><a href="{{ route('product', [15]) }}">Product Details</a></li>
                        <li><a href="{{ route('contact') }}">Contact</a></li>
                        <li><a href="{{ route('add.index') }}">Add Product</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>
    <div class="for_affix_padding"></div>
    <!-- /Category & search -->
</header>
<!-- /Header -->

{{-- Carousel --}}
@yield('carousel')
{{-- /Carousel --}}

<!-- Main -->
<div class="container">
    <div class="row">
        <!-- Sidebar -->
        @section('sidebar')
        <div class="col-xs-12 col-sm-3 sidebar">
            <aside>
                <h3>CATEGORY</h3>
                <div class="panel-group panel_category" id="accordion">
                    <div class="panel_links">
                        <ul>
                            @foreach($categories as $category)
                                <li><a href="{{route('category',['id'=>$category->id])}}">{{$category->title}}</a></li>
                            @endforeach
                        {{--</ul>--}}
                    </div>
                </div>
                
                <div class="advertisement text-center">
                    <img src="{{ asset('images/govazd.jpg') }}" alt="advertisement">
                </div>
            </aside>
        </div>
        @show
        <!-- /Sidebar -->
        <!-- Main Information -->
        @yield('content')
        <!-- /Main Information-->
    </div>
</div>
<!-- /Main -->


<!-- Footer -->
<footer>
    <!-- Footer main -->
    <div class="container-fluid footer_main">
        <!-- Footer top -->
        <div class="container ">
            <div class="row">
                <!-- Footer top info -->
                <div class="col-xs-12 col-sm-2">
                    <div class="footer_top_info">
                        <h1>IT-TALENTS</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                    </div>
                </div>
                <!-- /Footer top info -->
                <!-- Footer top videos -->
                <div class="col-xs-12 col-sm-6 ">
                    <div class="footer_top_icon_group">
                        <div class="col-xs-6 col-sm-3 footer_top_icon">
                            <a href="#">
                                <div>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <img src="{{ asset('images/iframe1.jpg') }}" alt="iframe1">
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <p>24 DEC 2014</p>
                        </div>
                        <div class="col-xs-6 col-sm-3 footer_top_icon">
                            <a href="#">
                                <div>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <img src="{{ asset('images/iframe2.jpg') }}" alt="iframe2">
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <p>24 DEC 2014</p>
                        </div>
                        <div class="col-xs-6 col-sm-3 footer_top_icon">
                            <a href="#">
                                <div>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <img src="{{ asset('images/iframe3.jpg') }}" alt="iframe3">
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <p>24 DEC 2014</p>
                        </div>
                        <div class="col-xs-6 col-sm-3 footer_top_icon">
                            <a href="#">
                                <div>
                                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                    <img src="{{ asset('images/iframe4.jpg') }}" alt="iframe4">
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <p>24 DEC 2014</p>
                        </div>
                    </div>
                </div>
                <!-- /Footer top videos -->
                <!-- Map -->
                <div class="col-xs-12 col-sm-4">
                    <div id="map" class="map"></div>
                </div>
                <!-- /Map -->
            </div>
        </div>
        <!-- /Footer top -->
        <!-- Footer middle -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-4">
                        <div class="footer_navigation">
                            <h4>SERVICE</h4>
                            <ul class="nav nav-stacked">
                                <li><a href="#">Online Help</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Order Status</a></li>
                                <li><a href="#">Change Location</a></li>
                                <li><a href="#">FAQ’s</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="footer_navigation">
                            <h4>QUOCK SHOP</h4>
                            <ul class="nav nav-stacked">
                                <li><a href="#">T-Shirt</a></li>
                                <li><a href="#">Mens</a></li>
                                <li><a href="#">Womens</a></li>
                                <li><a href="#">Gift Cards</a></li>
                                <li><a href="#">Shoes</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="footer_navigation">
                            <h4>POLICIES</h4>
                            <ul class="nav nav-stacked">
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Privecy Policy</a></li>
                                <li><a href="#">Refund Policy</a></li>
                                <li><a href="#">Billing System</a></li>
                                <li><a href="#">Ticket System</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="col-xs-4 col-md-5">
                        <div class="footer_navigation">
                            <h4>ABOUT SHOP</h4>
                            <ul class="nav nav-stacked">
                                <li><a href="#">Company Information</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Store Location</a></li>
                                <li><a href="#">Affillate Program</a></li>
                                <li><a href="#">Copyright</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-7 col-md-7">
                        <div class="pull-left footer_navigation">
                            <h4>ABOUT SHOP</h4>
                            <form class="navbar-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your email address">
                                </div>
                                <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"
                                                                                 aria-hidden="true"></i></button>
                                <p>Get the most recent updates from our site and be updated your self...</p>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Footer middle -->
    </div>
    <!-- /Footer main -->
    <!-- Bottom line -->
    <div class="container-fluid bottom_line">
        <div>
            <p class="pull-left">Copyright © 2013 IT-TALENTS All rights reserved.</p>
            <p class="pull-right">Designed by <a href="http://it-talents.org"> IT-TALENTS</a></p>
        </div>
    </div>
    <!-- /Bottom line -->
</footer>
<!-- /Footer -->

<!-- Login and Register modal -->
<div id="id_modalloginregister" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content modal_login_register">
            <div class="modal-header modal_header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <ul id="id_login_register_menu" class="nav nav-tabs login_register_menu">
                    <li id="id_login_header" class="active login_register_tab_button"><a href="#id_login_tab">Login</a>
                    </li>
                    <li id="id_register_header" class="login_register_tab_button">
                        <a href="#id_register_tab">Register</a>
                    </li>
                </ul>
            </div>
            <div id="id_tab_content" class="tab-content modal-body">

                {{-- Login form --}}
                <div class="tab-pane fade in active login_tab" id="id_login_tab">
                    <form method="POST" action="{{ route('login') }}" class="form-group" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="email" name="email" class="form-control input-lg" id="id_login_username" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control input-lg" id="id_login_password" placeholder="Password">
                        </div>
                        <input id="id_signed_in" type="checkbox">
                        <label for="id_signed_in">Keep me signed in</label>
                        <br>
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                        <button type="submit" class="btn  center-block login_btn">Sign in</button>
                    </form>
                </div>
                {{-- Login form --}}


                {{-- Register form --}}
                <div class="tab-pane fade register_tab" id="id_register_tab">
                    <form method="POST" action="{{ route('register') }}" class="form-group" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" name="name" class="form-control input-lg" id="id_register_username" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="text" name="surname" class="form-control input-lg" id="id_register_surname" placeholder="Surname">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control input-lg" id="id_register_email" placeholder="Email Address">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control input-lg" id="id_register_password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_confirmation" class="form-control input-lg" id="id_register_confirm_password" placeholder="Confirm password">
                        </div>
                        <button type="submit" class="btn center-block register_btn">Sign up</button>
                    </form>
                </div>
                {{-- Register form --}}

            </div>
        </div>
    </div>
</div>
<!-- /Login and Register modal -->

<!-- View picture modal -->
@yield('zoom-img')
<!-- /View picture modal -->

<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-slider.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-touch-slider-min.js') }}"></script>
<script src="{{ asset('owlcarousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/jquery.ez-plus.js') }}"></script>
<script src="{{ asset('js/swipe.js') }}"></script>
<script src="{{ asset('js/main-backup.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<!-- Google map -->
<script>
    function initMap() {
        var myLatLng = {lat: 40.189562, lng: 44.523454};
        var myLatLng2 = {lat: 41.189562, lng: 44.523454};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng,
            gestureHandling: 'cooperative',
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: true,
            rotateControl: false,
            fullscreenControl: false,
            styles: [
                {
                    "stylers": [
                        {
                            "hue": "#ff1a0a"
                        },
                        {
                            "invert_lightness": true
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 33
                        },
                        {
                            "gamma": 0.5
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#2D333C"
                        }
                    ]
                }
            ]
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Office1'
        });
        var marker2 = new google.maps.Marker({
            position: myLatLng2,
            map: map,
            title: 'Office2'
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDce-MAK41rJP1ND-vnrGl8A6R5KhoBGKw&callback=initMap">
</script>
<!-- /Google map -->
</body>
</html>